import {
  InnerBlocks,
  useBlockProps
} from '@wordpress/block-editor'

const blockJSON = require('../../../block-configs/accordion-block.json')

export const accordionBlockName = blockJSON.name

export const accordionBlockData = {
  name: blockJSON.name,
  apiVersion: blockJSON.apiVersion,
  title: blockJSON.title,
  icon: blockJSON.icon,
  category: blockJSON.category,
  edit: (props) => {

    const {
      attributes: {
        accordionID
      },
      setAttributes
    } = props

    const blockProps = useBlockProps();

    setAttributes({ accordionID: blockProps.id })

    console.log({ accordionID });

    return (
      <div {...blockProps}>
        <InnerBlocks 
          allowedBlocks={[
            blockJSON.allowedBlocks
          ]}
          template={
            [
              [
                'ucomm-blocks/accordion-block-item', 
                {
                  itemTitle: '',
                  itemContent: ''
                }
              ]
            ]
          }
          // add a button to add more accordion items
          renderAppender={InnerBlocks.ButtonBlockAppender}
        />
      </div>
    )

  },
  save: () => {
    return (
      <div { ...useBlockProps.save() }>
        <InnerBlocks.Content />
      </div>
    )
  }
}