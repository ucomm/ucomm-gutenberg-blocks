import {
  useBlockProps,
  PlainText,
  RichText
} from '@wordpress/block-editor'

const blockData = require('../../../block-configs/accordion-block-item.json')

export const accordionBlockItemName = blockData.name

export const accordionBlockItemData = {
  apiVersion: blockData.apiVersion,
  title: blockData.title,
  icon: blockData.icon,
  category: blockData.category,
  edit: (props) => {

    const blockProps = useBlockProps()

    const {
      attributes: {
        itemTitle,
        itemContent,
        itemID
      },
      setAttributes,
      context
    } = props

    
    const parentID = context
    console.log({ itemID, parentID });

    setAttributes({ itemID: blockProps.id })

    return (
      <div {...blockProps}>
        <div>
          <button
            style={{
              backgroundColor: 'inherit',
              border: '1px solid',
              padding: '8px 16px',
              width: '100%',
            }}
          >
            <PlainText 
              placeholder='Accordion Item Title'
              value={ itemTitle === '' ? '' : itemTitle }
              onChange={(newTitle) => setAttributes({ itemTitle: newTitle })}
              style={{ backgroundColor: 'inherit' }}
            />
          </button>
        </div>
        <div>
          <RichText 
            tagName='p'
            value={ itemContent }
            onChange={(newContent) => setAttributes({ itemContent: newContent })}
            style={{
              backgroundColor: 'inherit',
              padding: '0 16px'
            }}
          />
        </div>
      </div>
    )

  },
  save: ({ attributes }) => {

    const { itemTitle, itemContent, itemID } = attributes

    const savedProps = useBlockProps.save({
      className: 'ucomm-accordion-item-wrapper',
      id: itemID
    })

    return (
      <div {...savedProps}>
        <div
          className={[
            'ucomm-accordion-item-button'
          ].join('')}
        > 
          <button
            aria-expanded='false'
            aria-controls={`ucomm-accordion-panel-${savedProps.id}`}
          >
            { itemTitle }
          </button> 
        </div>
        <div
          id={`ucomm-accordion-panel-${savedProps.id}`}
          className={[
            'ucomm-accordion-panel'
          ].join(' ')}
          style={{ display: 'none' }}
        >
          <RichText.Content 
            tagName='p'
            value={ itemContent }
          />
        </div>
      </div>
    )
  }
}