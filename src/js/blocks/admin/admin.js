import { registerBlockType } from '@wordpress/blocks'
import { accordionBlockName, accordionBlockData } from '../accordion'
import { accordionBlockItemData, accordionBlockItemName } from '../accordion-item'

registerBlockType(accordionBlockName, accordionBlockData)
registerBlockType(accordionBlockItemName, accordionBlockItemData)
