<?php

namespace UComm_Blocks\Assets;

abstract class Loader {
  protected $buildDir;

  public function __construct()
  {
    $this->buildDir = $_SERVER['HTTP_HOST'] === 'localhost' ? 'dev-build' : 'build';
  }

  public function enqueueAssets() {
    add_action('wp_enqueue_scripts', [ $this, 'enqueue' ]);
  }

  public function enqueueAdminAssets() {
    add_action('admin_enqueue_scripts', [ $this, 'adminEnqueue' ]);
  }

  abstract function enqueue();

  abstract function adminEnqueue();

  abstract function registerPublicAssets();

  abstract function registerAdminAssets();

  protected function getAssetFile(string $name) {
    return include UCOMM_GUTENBERG_PLUGIN_DIR . $this->buildDir . '/' . $name;
  }

  protected function getAssetPath(string $file): string {
    return UCOMM_GUTENBERG_PLUGIN_URL . $this->buildDir . '/' . $file;
  }
}