<?php

namespace UComm_Blocks\Assets;

use UComm_Blocks\Assets\Loader;

class ScriptLoader extends Loader {

  public function enqueue() {
    $this->registerPublicAssets();
  }

  public function adminEnqueue() {
    $this->registerAdminAssets();
  }

  public function registerPublicAssets()
  {
    $accordionAssets = $this->getAssetFile('accordion.asset.php');
    $accordionItemAssets = $this->getAssetFile('accordionItem.asset.php');

    $accordionItemDeps = array_push($accordionItemAssets['dependencies'], 'accordion-block-script');

    wp_register_script(
      'accordion-block-script',
      $this->getAssetPath('accordion.js'),
      $accordionAssets['dependencies'],
      $accordionAssets['version'],
      true
    );

    wp_register_script(
      'accordion-block-item-script',
      $this->getAssetPath('accordionItem.js'),
      $accordionItemDeps,
      $accordionAssets['version'],
      true
    );
  }

  public function registerAdminAssets() {
    $blockAdminAssets = $this->getAssetFile('blockAdmin.asset.php');
    wp_register_script(
      'block-admin-script',
      $this->getAssetPath('blockAdmin.js'),
      $blockAdminAssets['dependencies'],
      $blockAdminAssets['version'],
      true
    );
  }
}