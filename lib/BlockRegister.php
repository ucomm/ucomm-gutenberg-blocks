<?php

namespace UComm_Blocks;

class BlockRegister {
  public $configDir;

  public function __construct()
  {
    $this->configDir = UCOMM_GUTENBERG_PLUGIN_DIR . 'block-configs'; 
  }

  public function init() {
    add_action('init', [ $this, 'registerBlocks' ]);
  }

  public function registerBlocks() {
    $blocks = [
      'accordion-block' => [
        'script' => 'accordion-block-script',
        'editor_script' => 'block-admin-script'
      ],
      'accordion-block-item' => [
        'script' => 'accordion-block-item-script',
        'editor_script' => 'block-admin-script'
      ],
    ];

    foreach ($blocks as $name => $config) {
      $this->registerBlock($name, $config);
    }

  }

  public function registerBlock(string $name, array $config = []) {

    $file = $this->configDir . '/' . $name . '.json';

    return register_block_type_from_metadata($file, $config);
  }

  public function mergeBlockCategories(array $categories): array {
    return array_merge(
      [
        [
          'slug' => 'ucomm-blocks',
          'title' => __('UConn Communications Blocks', 'ucomm-gutenberg-blocks'),
          'icon' => 'wordpress'
        ]
      ],
      $categories
    );
  }

  public function filterBlockCategories() {
    add_filter('block_categories', [ $this, 'mergeBlockCategories'], 10, 2);
  }
}

