const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const DependencyExtractionPlugin = require('@wordpress/dependency-extraction-webpack-plugin')

const prodEnv = process.env.NODE_ENV === 'prodution' ? true : false

let mode = 'development'
let buildDir = path.resolve(__dirname, 'dev-build')

if (prodEnv) {
  mode = 'production'
  buildDir = path.resolve(__dirname, 'build')
  optimization.minimize = true
  optimization.minimizer = [
    new CssMinimizerPlugin(),
    '...'
  ]
}

module.exports = {
  entry: {
    main: path.resolve(__dirname, 'src', 'js', 'index.js'),
    blockAdmin: path.resolve(__dirname, 'src', 'js', 'blocks', 'admin', 'admin.js'),
    accordion: path.resolve(__dirname, 'src', 'js', 'blocks', 'accordion.js'),
    accordionItem: path.resolve(__dirname, 'src', 'js', 'blocks', 'accordion-item.js'), 
  },
  output: {
    path: buildDir,
    filename: '[name].js',
    chunkFilename: '[name][ext]',
  },
  mode,
  module: {
    rules: [
      {
        test: /\.(ts|tsx|js|jsx)$/,
        exclude: /node_modules/,
        use: [
          // {
          //   loader: 'ts-loader'
          // },
          {
            loader: 'babel-loader',
            options: {
              presets: [
                '@babel/preset-typescript',
                [
                  '@babel/preset-env',
                  {
                    debug: true,
                    useBuiltIns: 'usage',
                    corejs: 3.21
                  }
                ],
                '@babel/preset-react'
              ]
            }
          }
        ]
      },
      {
        test: /\.s?css$/,
        exclude: /node_modules/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              importLoaders: 2
            }
          },
          {
            loader: 'postcss-loader'
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              sassOptions: {
                includePaths: [
                  'node_modules/scss/**/*.scss'
                ]
              }
            }
          }
        ]
      },
    ]
  },
  resolve: {
    extensions: [ '.ts', '.tsx', '.js', '.jsx', '.css', '.scss' ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css',
    }),
    new DependencyExtractionPlugin({
      useDefaults: true
    }),
  ]
}