<?php

/*
Plugin Name: University Communications Gutenberg Blocks (General)
Description: Gutenberg blocks used across University Communications sites
Author: UComm Web Team
Version: 0.0.1
Text Domain: ucomm-gutenberg-blocks
*/

if (!defined('WPINC')) {
  die;
}

use UComm_Blocks\Assets\ScriptLoader;
use UComm_Blocks\BlockRegister;

define( 'UCOMM_GUTENBERG_PLUGIN_DIR', plugin_dir_path(__FILE__) );
define( 'UCOMM_GUTENBERG_PLUGIN_URL', plugins_url('/', __FILE__) );

require 'lib/BlockRegister.php';
require 'lib/Assets/Loader.php';
require 'lib/Assets/ScriptLoader.php';

$scriptLoader = new ScriptLoader();
$register = new BlockRegister();

$scriptLoader->enqueueAssets();
$scriptLoader->enqueueAdminAssets();


$register->filterBlockCategories();
$register->init();
