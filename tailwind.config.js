module.exports = {
  content: [
    './src/js/**/*.js',
    './src/js/**/*.jsx'
  ],
  theme: {
    extend: {},
  },
  plugins: [],
  corePlugins: {
    preflight: false
  }
}
